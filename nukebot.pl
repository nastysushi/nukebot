use strict;
    use vars qw($VERSION %IRSSI @BOTMASTERS $ALLOW_SUBMISSION $SJW_DBFILE @DEFAULT_SJWS @SJWS $SJWS_COUNT);
     
    use Irssi;
     
    $VERSION = '9.11';
    %IRSSI = (
        authors     => 'Master Sushi & George W. Fucking Bush',
        contact     => 'Don\'t',
        name        => 'Linkbot',
        description => 'Capable of randomized link sending',
        license     => 'Public Domain',
    );
    #set the source url where the code is hosted at :)
    my $SOURCE = "https://gitlab.com/nastysushi/nukebot";
    
    # List of individuals who can administrate and debug Linkbot.
    @BOTMASTERS = ("Matsurishi", "gwbush");
     
    # Allow target submissions.
    $ALLOW_SUBMISSION = 1;
     
    # File containing CSV database of targets.
    $SJW_DBFILE = "sjw.db";
     
    # Default target list for initial population of the database.
    @DEFAULT_SJWS = ("http://www.swivets.tumblr.com", "http://chubby-bunnies.tumblr.com/", "http://rebeccasugar.tumblr.com/", "http://mollymodest.tumblr.com/",
        "http://wilsonsaskani.tumblr.com/", "http://kurloz-dream-bubble.tumblr.com/", "http://ironic-knight-of-time.tumblr.com/",
        "http://p-l-a-s-t-i-c-b-i-t-c-h.tumblr.com/", "http://emmberzander.tumblr.com/", "http://intergalacticdragon.tumblr.com/",
        "http://pantronsaintofpapercuts.tumblr.com", "http://xanderverse.tumblr.com/", "http://wyylie.tumblr.com/", "http://edens-blog.tumblr.com/",
        "http://drunk-on-blades.tumblr.com/", "http://sjwvevo.tumblr.com/", "http://creamyfaerie.tumblr.com/", "http://afro-lannister.tumblr.com/",
        "http://frankiestein-the-freak.tumblr.com/", "http://johnwatsonschafingpenis.tumblr.com/", "http://fetsu-chan.tumblr.com/",
        "http://mahouprince.tumblr.com/", "http://pastelbat.tumblr.com/", "http://cadney.tumblr.com/", "http://lacecommunity.tumblr.com/",
        "http://pudinsesu.tumblr.com/", "http://petitepasserine.tumblr.com/", "http://princess-peachie.tumblr.com/", "http://afatfox.tumblr.com/",
        "http://tesshollidayofficial.tumblr.com/", "http://bcbeccabae.tumblr.com/", "http://queenmerbabe.tumblr.com/", "http://bronzebun.tumblr.com",
        "http://fistarnius.tumblr.com/", "http://julizio.tumblr.com/", "http://kantr.tumblr.com/", "http://slayboybunny.tumblr.com/", "http://bunbijou.tumblr.com/",
        "http://demib0y.tumblr.com/", "http://plaidkin.tumblr.com/", "http://righthandedlink.tumblr.com/", "http://curvellas.tumblr.com/",
        "http://kin-assistance.tumblr.com", "http://idolgender.tumblr.com/", "http://curvesincolor.tumblr.com/", "http://hatsfilms.tumblr.com/",
        "http://prominence.tk/", "http://dragondicks.tumblr.com/", "http://interr0.tumblr.com/", "http://littlesokka.tumblr.com/", "http://cubistemoji.tumblr.com/",
        "http://kin-experiences.tumblr.com/", "http://transgemder.tumblr.com/", "http://hexygen.tumblr.com/", "http://fictionfinds.tumblr.com/canons",
        "http://lgbtwarriorcats.tumblr.com/", "http://vloggswithme.tumblr.com/");
     
    # Runtime target list.
    @SJWS;
     
    # Size of the target list.
    $SJWS_COUNT;
     
     
    sub load_sjws {
        # Check if the SJW database exists and is non-zero.
        if(-s $SJW_DBFILE) {
            # Load up the lard if the database is found.
            open my $fd, "<", $SJW_DBFILE or die "Check the permissions or filesystem for $!.";
            my @lines = <$fd>;                      # Read all lines of the database into an array
            @SJWS = split(',', join('', @lines));   # Join all likes of the database, split the CSV, and push into SJWS
            close $fd;                              # Tell the file to fuck off
            save_sjws();
        }
        else {
            # Go to defaults if the database is not found.
            @SJWS = @DEFAULT_SJWS;                  # Populate with defaults
            save_sjws();                            # Commit to the new database
        }
       
        # Start the target counter
        $SJWS_COUNT = scalar(@SJWS);
    }
     
    sub save_sjws {
        open my $fd, ">", $SJW_DBFILE or die "Check the permissions or filesystem for $!.";
        print $fd join(',', @SJWS);                 # Create the CSV by joining with commas
        close $fd;                                  # Tell the file to fuck off
    }
    
    sub send_rules {
        my ($server,$channel) = @_;
        #$channel = substr($channel,
        $server->command("msg $channel Rules:");
        $server->command("msg $channel 1: Read the fucking topic. Failure to do this will result in you being banned. Yes, you topcat.");
    }
    sub send_help {
        my ($server, $nick) = @_;
       
        $server->command("msg $nick Linkbot Version 9.11 Command List");
        $server->command("msg $nick IRC commands: !help - You just did it | !board - Board link | !blogs - Opnei blog list | !sjw - Random SJW blog link | !sjwcount - Gives the total number of targets | !add <url> - Adds the specified URL into the SJW target list | !sargon - Latest and weekly Sargon vid | !source - Bot sourcecode link | !botmasters - Lists my massas");
       
        # Botmaster commands (only appear to botmasters)
        if ($nick ~~ @BOTMASTERS) {
            $server->command("msg $nick Botmaster commands: !dump - Dumps the full list of SJWs | !del <url> - Removes the specified URL from the SJW list. | !msg user/channel - Send a message from linkbot to user/channel. | !kkk - KKK ASCII LOGOS | !ntarget - target user for kick | !ntoggle - RIP user");
        }
    }
    my $isNuke = 0;
    my @target = "";
    my @tall = 0;
    sub event_privmsg {
        my ($server, $data, $nick, $mask, $target) = @_;
        my ($target, $text) = $data =~ /^(\S*)\s:(.*)/;
       
       
        if ($text =~ /^!board$/i) {
            # Fullchan board link
            $server->command("msg $nick Board: http://www.8ch.net/opnei/");
        }
       	elsif ($text =~ /(:3)/i || $text =~ /(:.3)/i || $text =~ /(:..3)/i || $text =~ /(:s)/i || $text =~ /(;3)/i || $text =~ /(;.3)/i || $text =~ /(;..3)/i || $text =~ /(X.3)/i || $text =~ /(X..3)/) {
	    my ($server,$channel,$nick)=@_;
	    $server->command("msg Chanserv kick #opnei $nick Cancerous face: $1");
	}
	elsif ($text =~ /(trans)/i || $text =~ /(patriarchy)/i || $text =~ /(can I have op)/i || $text =~ /(have op)/i || $text =~ /(give me op)/i || $text=~/(give op)/){
	    my ($server,$channel,$nick)=@_;
	    $server->command("msg Chanserv kick #opnei $nick You: $1 Me: Go fuck yourself");
	}
        elsif ($text =~ /^!help$/i) {
            # Help command
            send_help($server, $nick);
        }
        elsif ($text =~ /^!blogs$/i) {
            # Opnei blogs list
            $server->command("msg $nick Opnei blogs: http://www.8ch.net/opnei/list.html");
        }
        elsif ($text =~ /^!ntarget\s+([A-Za-z\d\-_]+)$/i && $nick ~~ @BOTMASTERS) {
            my ($server,$channel2)=@_;
	    @target = $1;
	    if("all" =~ @target) {
	        $server ->command("msg $nick @target targeted.");
                @tall = 1;
            }
            else {
                $server->command("msg $nick specific user @target targeted.");
            }
        }
        elsif ($text =~ /^!rules$/i) {
            my ($server,$channel)=@_;
            send_rules($server,$channel);
        }
        elsif ($isNuke) {
            my ($server,$channel,$nick)=@_;
            if (@tall) {
                if ($nick ~~ @BOTMASTERS) {
                   my ($server,$channel,$nick)=@_;
                   $server->command("msg $nick spared from nuke. Thank the botmaster.");
                }
                else {
                   my ($server,$channel,$nick)=@_;
                   $server->command("msg chanserv kick #opnei $nick Git mened son X-DDDD");
                }
            }
            elsif ($nick =~ @target){
                $server->command("msg chanserv kick #opnei $nick shrekt(NUKEMODE:True");
            }
        }
        elsif ($text =~ /^!ntoggle$/i && $nick ~~ @BOTMASTERS) {
          if ($isNuke == 0) {
                $isNuke = 1;
                $server->command("msg $nick Nuke toggled on.");
          }
          elsif ($isNuke == 1) {
                $isNuke = 0;
                $server->command("msg $nick Nuke toggled off.");
          }      
        }
        elsif ($text =~ /^!sjw$/i) {
            # Random link to a SJW blog
            my $range = @SJWS[int(rand($SJWS_COUNT))];
            $server->command("msg $nick Random SJW blog: $range");
        }
        elsif($text =~ /^!kick\s+([A-Za-z\d\-_]+)$/i && $nick ~~ @BOTMASTERS) {
			my ($server,$channel)=@_;
			my $user = substr($channel,14);
			$server->command("msg ChanServ kick #opnei $user an faggot");
		}
		elsif($text =~ /^!op\s+([A-Za-z\d\-_]+)$/i && $nick ~~ @BOTMASTERS) {
			my ($server,$channel)=@_;
			my $user = substr($channel,15);
			my $chan = "#raidchan";
			$server->command("msg ChanServ OP #raidchan $user");
		}
        elsif ($text =~ /^!sjwcount$/i) {
            # Total number of targets
            $server->command("msg $nick We have $SJWS_COUNT targets.");
        }
        elsif ($text =~ /^!sargon$/i) {
            # Sargon (????)
            $server->command("msg $nick [Latest: https://www.youtube.com/watch?v=WhWsDkqrofw ] [Weekly: https://www.youtube.com/watch?v=-uVvVbczN_A ]");   
        }
        elsif($text =~ /^!source$/i) {
            # Send the URL to the hosted sourcecode.
            $server->command("msg $nick Bot sourcecode: $SOURCE");
        }
        elsif($text =~ /^!botmasters$/i) {
            # Lists the botmasters
            $server->command("msg $nick Botmasters: " . join(', ', @BOTMASTERS));
        }
        elsif($text =~ /^!kkk$/i && $nick ~~ @BOTMASTERS){
			my($server,$channel)=@_;
		    my $chan = substr($channel,0,-5);
			#send ascii to chat
			$server->command("msg #opnei ██ ▄█▀ ██ ▄█▀ ██ ▄█▀");
			$server->command("msg #opnei ██▄█▒  ██▄█▒  ██▄█▒");
			$server->command("msg #opnei ▓███▄░ ▓███▄░ ▓███▄░");
			$server->command("msg #opnei ▓██ █▄ ▓██ █▄ ▓██ █▄");
			$server->command("action #opnei eats black babies");
		}
        elsif($text =~ /^!addtempmaster\s+([A-Za-z\d\-_]+)$/i && $nick ~~ @BOTMASTERS) {
            # Adds a temporary botmaster
            my $master = $1;
            push(@BOTMASTERS, $master);         # Add the master to the list of Botmasters.
     
            $server->command("msg $nick Added $master to the list of Botmasters. They will be removed upon shutdown. If you want to remove them faster use !deltempmaster. If you want to keep them, add them in the source of this script.");
            $server->command("msg $master Hello massa, $nick made you one of my Botmasters.");
        }
        elsif($text =~ /^!deltempmaster\s+([A-Za-z\d\-_]+)$/i && $nick ~~ @BOTMASTERS) {
            # Removes a temporary botmaster
            my $master = $1;
            my $botmastersCount = scalar(@BOTMASTERS);
            my $index = 0;
           
            # Iterate over the Botmasters until the master is found or the array is exceeded.
            $index++ until @BOTMASTERS[$index] eq $master || $index > $botmastersCount;
           
            if($index > $botmastersCount) {
                $server->command("msg $nick Could not find $master in the Botmasters list. Check !botmasters.");
            }
            else {
                splice(@BOTMASTERS, $index, 1);   # Removes the master from the Botmasters array
                $server->command("msg $nick Removed $master from the list of Botmasters.");
                $server->command("msg $master You have been removed from my list of Botmasters.");
            }
        }


        elsif($text =~ /^!add\s+(.*)$/i) {
            if($ALLOW_SUBMISSION) {
                my $target = $1;
                my $username = $target;
               
                if($target =~ /(?:https?:\/\/)?(?:www.)?([A-Za-z\d\-_]+).tumblr.com.*$/i) {
                    # Detected URL format
                    $username = $1;
                }
                else {
                    $username = $target;
                }
               
                if($username =~ /([A-Za-z\d\-_]+)$/i) {
                    push(@SJWS, "http://$username.tumblr.com/");    # Adds the target to the memory array
                    $SJWS_COUNT++;                                  # Increments the total count of targets for the !sjw command
                    save_sjws();                                    # Commits the resulting memory array to disk
                    $server->command("msg $nick Successfully added target, $username, to the list.");
                }
                else {
                    $server->command("msg $nick Failed to verify link - result was $target. If you believe this is an error speak to the botmasters: " . join(', ', @BOTMASTERS));
                }
            }
            else {
                $server->command("msg $nick SJW target submissions are disabled.");
            }
        }
        elsif($text =~ /^!dump$/i && $nick ~~ @BOTMASTERS) {
            # Whaledump
            $server->command ("msg $nick " . join(', ', @SJWS));
        }
        elsif($text =~ /^!del\s+(.*)$/i && $nick ~~ @BOTMASTERS) {
            my $index = 0;
            my $target = $1;
           
            # Iterate over the memory array until the target is found or the size of the array is exceeded
            $index++ until @SJWS[$index] eq $target || $index > $SJWS_COUNT;
           
            if($index > $SJWS_COUNT) {
                $server->command("msg $nick Could not find $target in the list. Please use the EXACT URL for this command. Use !dump to see the current list.");
            }
            else {
                splice(@SJWS, $index, 1);   # Removes the target from the memory array
                $SJWS_COUNT--;              # Decrements the total count of targets for the !sjw command
                save_sjws();                # Commits the resulting memory array to disk
                $server->command("msg $nick Successfully removed $target from index $index.");
            }
        }
        elsif($text =~ /^!msg\s+(#?[A-Za-z\d\-_]+) (.*)$/i && $nick ~~ @BOTMASTERS) {
            my $msgTo = $1;
            my $msgText = $2;
           
            $server->command("msg $msgTo $msgText");
            $server->command("msg $nick Sent '$msgText' to $msgTo.");
        }
        elsif ($text =~ /^!/i) {
            # When in doubt, send help
            send_help($server, $nick);
        }
    }
     
    # Populate memory with SJWs.
    load_sjws();
     
    Irssi::signal_add('event privmsg', 'event_privmsg');
    Irssi::command_bind("ln", "linkify");
    1;